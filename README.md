# README #

Facturat para administrar tu gasto hecha en net core y angular -4-

### Instruccion ###

Levantar app angular:

```shell
ng serve --proxy-config proxy.config.json
```

Levantar app netcore:
```shell
	dotnet watch run
```

o

```shell
dotnet -d watch run
```